#!/bin/bash

if [ `dpkg -l | grep nodejs | awk -F ' ' 'NR==1{print $1}'` ]; 
then 
echo "*** NodeJS is Available ***"; 
else 
echo "*** Installing NodeJS ***";
sudo apt-get update -qq > /dev/null
sudo apt-get -qq install nodejs > /dev/null
fi

if [ `dpkg -l | grep npm | awk -F ' ' 'NR==1{print $1}'` ]; 
then 
echo "*** NPM is Available ***"; 
else 
echo "*** Installing NPM ***";
sudo apt-get -qq install npm > /dev/null
fi

pkill node

if [ -d app ]
then 
rm -rf app
fi
tar -xzf $BUILD_TAG.tar.gz 
mv $BUILD_TAG app
cd app
npm install 

nohup node node_modules/react-scripts/scripts/start.js 1> react.out 2> react-2.out &

echo "Done with Code Deployment"